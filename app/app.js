import Vue from "nativescript-vue";
import App from "./components/App";
import Home from "./components/Home";
import DrawerContent from "./components/DrawerContent";
import RadSideDrawer from "nativescript-ui-sidedrawer/vue";
import store from '../core/store-mobile'
Vue.use(RadSideDrawer);

Vue.registerElement('VideoPlayer', () => require('nativescript-videoplayer').Video)

Vue.config.silent = (TNS_ENV === 'production');
require("nativescript-localstorage");

new Vue({
    render (h) {
        return h(
          App,
          [
            h(DrawerContent, { slot: 'drawerContent' }),
            h(Home, { slot: 'mainContent' })
          ]
        )
      },
      store,
      async mounted() {
      },
      computed: {
      }
  }).$start();
